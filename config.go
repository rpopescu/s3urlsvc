package main

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// Config object
type Config struct {
	// Port used by the HTTP frontend
	Port int
	// Validity duration of pre-signed URLs (in seconds)
	Duration int
	S3       S3Config
}

// S3Config contains S3 configuration options
type S3Config struct {
	Credentials    string
	Endpoint       string
	Region         string
	ForcePathStyle bool `mapstructure:"force_path_style"`
	DisableSSL     bool
}

// ReadConfig read configuration files and populate a Config object
func ReadConfig() (*Config, error) {
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/s3urlsvc")
	viper.AddConfigPath("$HOME/.s3urlsvc")
	viper.AddConfigPath("./config")

	viper.SetDefault("port", 8080)
	viper.SetDefault("duration", 60)
	viper.SetDefault("s3.force_path_style", false)
	viper.SetDefault("s3.disableSSL", false)

	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "Could not read config file")
	}
	var conf Config
	if err := viper.Unmarshal(&conf); err != nil {
		return nil, errors.Wrap(err, "Could not parse S3 configuration")
	}

	return &conf, nil
}
