package main

import (
	"log"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/service/s3"
)

// SignURLHandler responds with pre-signed URLs that can be used for uploading
type SignURLHandler struct {
	s3Client *s3.S3
	duration int
}

func (h SignURLHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	bucket := req.URL.Query().Get("bucket")
	key := req.URL.Query().Get("key")

	input := s3.PutObjectInput{
		Bucket: &bucket,
		Key:    &key,
	}
	s3Req, _ := h.s3Client.PutObjectRequest(&input)

	str, err := s3Req.Presign(time.Duration(h.duration) * time.Second)
	if err != nil {
		msg := "Could not sign URL"
		log.Println(msg, err)
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}

	w.Write([]byte(str))
}
