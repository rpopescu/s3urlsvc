# S3 URL Service

## Overview

A web service for providing pre-signed URLs for uploading into S3 buckets

## License and copyright

See LICENSE in the project root.
