package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gorilla/mux"
)

func main() {
	conf, err := ReadConfig()
	if err != nil {
		log.Fatalln("Could not read configuration:", err)
	}

	awsCreds := credentials.NewSharedCredentials(conf.S3.Credentials, "default")

	awsCfg := aws.NewConfig().
		WithCredentials(awsCreds).
		WithRegion(conf.S3.Region).
		WithEndpoint(conf.S3.Endpoint).
		WithS3ForcePathStyle(conf.S3.ForcePathStyle).
		WithDisableSSL(conf.S3.DisableSSL)

	sesh, err := session.NewSession(awsCfg)
	if err != nil {
		log.Fatalln("Could not initialize S3 session:", err)
	}

	router := mux.NewRouter()
	r := router.NewRoute()
	r.Path("/sign")
	r.Methods("GET")
	r.Queries("bucket", "", "key", "")
	r.Handler(SignURLHandler{s3.New(sesh), conf.Duration})

	srv := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf(":%d", conf.Port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Starting HTTP frontend")

	if err := srv.ListenAndServe(); err != nil {
		log.Fatalln("Could not start HTTP frontend:", err)
	}
}
